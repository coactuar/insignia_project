<?php
require_once "../functions.php";
$title = 'Insignia';

$fb = new Feedback();
$fbList = $fb->getFeedbacks();
//var_dump($fbList);

$i = 0;
$data = array();
$member = new User();
if (!empty($fbList)) {
  foreach ($fbList as $user) {
    $member->__set('user_id', $user['userid']);
    $userInfo = $member->getUser();

    $data[$i]['Name'] = $userInfo[0]['first_name'] . ' ' . $userInfo[0]['last_name'];
    $data[$i]['E-mail ID'] = $userInfo[0]['emailid'];
    $data[$i]['Please rate the overall aspects of this educational activity based on:'] = $user['q26'];
    $data[$i]['Dr Betul Hatipoglu'] = $user['q05'];
    $data[$i]['Dr Rita Basu'] = $user['q06'];
    $data[$i]['Dr Dennis Bruemmer'] = $user['q07'];
    $data[$i]['Dr Spyridoula Maraka '] = $user['q08'];
    $data[$i]['Dr Anand Vaidya '] = $user['q09'];
    $data[$i]['Dr Natalie Cusano'] = $user['q10'];
    $data[$i]['Dr Richard Auchus'] = $user['q11'];
    $data[$i]['Dr Maria Fleseriu'] = $user['q12'];
    $data[$i]['Dr Betul Hatipoglu'] = $user['q01'];
    $data[$i]['Dr Rita Basu'] = $user['q02'];
    $data[$i]['Dr Dennis Bruemmer '] = $user['q03'];
    $data[$i]['Dr Spyridoula Maraka '] = $user['q04'];
    
    $data[$i]['Evaluation and Management of Suspicious Adrenal Masses'] = $user['q13'];
    $data[$i]['Mild Forms of Hyperparathyroidism [Including Normocalcemic]'] = $user['q14'];
    $data[$i]['Approach to Patient with Adrenal Insufficiency'] = $user['q15'];
    $data[$i]['Personalized Treatment in Acromegaly'] = $user['q16'];
   
   
    $data[$i]['What influenced you to attend this meeting?'] = $user['q27'];
    $data[$i]['Do you have any other feedback?'] = $user['q28'];
    // $data[$i]['Feedback Time'] = $user['q29'];
    $data[$i]['Feedback Time'] = $user['feedback_time'];
    $i++;
  }
}
$filename = $title . "_feedbacks.xls";
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=\"$filename\"");
ExportFile($data);
